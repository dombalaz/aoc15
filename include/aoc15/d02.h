#pragma once

#include <cstdint>

using uint = std::uint32_t;

auto paperForGift(uint x, uint y, uint z) -> uint;

auto areaOfSmallest(uint x, uint y, uint z) -> uint;

auto ribbonForGift(uint x, uint y, uint z) -> uint;
