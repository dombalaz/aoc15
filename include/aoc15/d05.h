#pragma once

#include <string>
#include <vector>

auto containsAtLeast3Vowels(const std::string &in) -> bool;

auto containsOneLetterTwiceInRow(const std::string &in) -> bool;

auto doesntContainsTheStrings(const std::string &in) -> bool;

auto isNice(const std::string &in) -> bool;

auto twoPairsOfTwoLetters(const std::string &in) -> bool;

auto repeatedLetterWithLetterBetween(const std::string &in) -> bool;

auto isNice2(const std::string &in) -> bool;
