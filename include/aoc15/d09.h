#pragma once

#include <string>
#include <vector>

auto shortestDistance(const std::vector<std::string> &v) -> size_t;

auto longestDistance(const std::vector<std::string> &v) -> size_t;
