#pragma once

#include <string>

auto containsIncreasingStraightOf3Letters(const std::string &in) -> bool;

auto doesntContainsLetters(const std::string &in) -> bool;

auto containsLetterPairs(const std::string &in) -> bool;

auto nextPass(const std::string &in) -> std::string;

auto nextValidPass(const std::string &in) -> std::string;
