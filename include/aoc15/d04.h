#pragma once

#include <string>

auto miningNumber(const std::string &key, size_t leadingZeroes) -> int;
