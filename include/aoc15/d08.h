#pragma once

#include <string>

auto countCharacters(const std::string &s) -> size_t;

auto countEncodedChars(const std::string &s) -> size_t;
