#pragma once

#include <string>

auto lookAndSay(const std::string &s) -> std::string;
