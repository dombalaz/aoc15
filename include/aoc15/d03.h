#pragma once

#include <cstdint>
#include <string>

using uint = std::uint32_t;

auto visitedHousesBySanta(const std::string &in) -> uint;

auto visitedHousesBySantaAndRobot(const std::string &in) -> uint;
